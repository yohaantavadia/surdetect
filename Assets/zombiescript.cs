﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zombiescript : MonoBehaviour {

    Animator anim;
   public  Transform player;
    float MoveSpeed = 4;
    float  MaxDist = 10;
    float MinDist = 5;
    bool run;
    void Start ()
    {
        anim = GetComponent<Animator>();
        StartCoroutine(Example());
    }


    void Update()
    {
        {
            transform.LookAt(player);


            if (Vector3.Distance(transform.position, player.position) >= MinDist && run == true)
            {

                transform.position += transform.forward * MoveSpeed * Time.deltaTime;



                if (Vector3.Distance(transform.position, player.position) <= MaxDist)
                {
                    anim.SetBool( "dance", true);
                }

            }


        }
    }
  
      
    

    IEnumerator Example()
    {
        yield return new WaitForSeconds(1);
        run = true;
        anim.SetBool("run", true);
    }
}
